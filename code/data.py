#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 12 16:12:16 2019

@author: Kai Niu
"""


import lxml.etree as et
import math
import numpy as np
import collections
import re
import random
from bs4 import BeautifulSoup
from bs4 import NavigableString
import pickle
from nltk.corpus import wordnet as wn
from nltk.corpus.reader.wordnet import WordNetCorpusReader
from nltk.stem import WordNetLemmatizer
from config import *
wordnet_lemmatizer = WordNetLemmatizer()  # download wordnet: import nltk; nltk.download("wordnet") in readme.txt


#_path = path.WSD_path()
#wn = WordNetCorpusReader(_path.WORDNET_PATH, '.*')
#print('wordnet version %s: %s' % (wn.get_version(), _path.WORDNET_PATH))

path_words_notin_vocab = '../tmp/words_notin_vocab_{}.txt'
config = MemNNConfig()
pos_dic = {
    'ADJ': u'a',
    'ADV': u'r',
    'NOUN': u'n',
    'VERB': u'v', }

POS_LIST = pos_dic.values()  # ['a', 'r', 'n', 'v']

def load_all_words_data(data_path, key_path=None, is_training=False):
    word_count_info = {}

    id_to_sensekey = {}
    if key_path:
        for line in open(key_path).readlines():
            id = line.split()[0]
            sensekey = line.split()[1]  # multiple sense
            id_to_sensekey[id] = sensekey
#    print(id_to_sensekey)
    context = et.iterparse(data_path, tag='sentence')

    data = []
    poss = set()
    for event, elem in context:
        sent_list = []
        pos_list = []
        for child in elem:
            word = child.get('lemma').lower()
            sent_list.append(word)
            pos = child.get('pos')
            pos_list.append(pos)
            poss.add(pos)

        i = -1
        pos_id_list = []
        sensekey_list = []
        target_word_list = []
        for child in elem:
            if child.tag == 'wf':
                i += 1
            elif child.tag == 'instance':
                i += 1
                id = child.get('id')
                lemma = child.get('lemma').lower()
                if '(' in lemma:
                    print( id)
                pos = child.get('pos')
                word = lemma + '#' + pos_dic[pos]
                if key_path:
                    sensekey = id_to_sensekey[id]
                else:
                    sensekey = None
                if is_training:
                    if word_count_info[word][0] <= 1 or word_count_info[word][1] <= 1:
                        continue
                pos_id_list.append(i)
                sensekey_list.append(sensekey)
                # I only treat the lemma 
                target_word_list.append(lemma)
                
        assert len(pos_id_list) == len(sensekey_list)
        
        time = 0
        while len(sent_list) > 0:
            print(sent_list)
            pos_id_list_tmp = []
            sent_list_tmp = sent_list[: config.maximum_setence_length]
            sensekey_list_tmp = []
            target_word_list_tmp = []
            pos_list_tmp = pos_list[: config.maximum_setence_length]
            
            for index,value in enumerate(pos_id_list):
                if config.maximum_setence_length * time <= value < config.maximum_setence_length * (time + 1):
                    pos_id_list_tmp.append(value - config.maximum_setence_length * time)
                    sensekey_list_tmp.append(sensekey_list[index])
                    target_word_list_tmp.append(target_word_list[index])
                    
            
            pos_list = pos_list[config.maximum_setence_length * (time + 1):]
            sent_list = sent_list[config.maximum_setence_length * (time + 1):]
            time += 1
            
            assert len(pos_id_list_tmp) == len(sensekey_list_tmp)
            assert len(sent_list_tmp) == len(pos_list_tmp)
            x = {
                'target_word_pos_id_list': pos_id_list_tmp,
                'context': sent_list_tmp,
                'target_sense': sensekey_list_tmp,  # don't support multiple answers
                'target_word': target_word_list_tmp,
                'poss': pos_list_tmp,
            }

            data.append(x)


    return data


def get_correct_sense_index(target_words, target_senses, target_word_to_senses):
    """
    :param 
        target_words:
            a list containing all the 
        target_senses: 
            a list containing all the correct sense corresponding to the target word
        target_word_to_senses:
            a dict mapping from word to all the potential senses
        sense_vocab:
            a dict mapping from sense to sense_id
    :return: 
        a list containing the index for the target word in all potential senses
    """
    result = []
    unknown_word = []
    unkonwn_sense = []
    for word, sense in zip(target_words,target_senses):
        if word not in target_word_to_senses:
            # assume all the target_word
            id = 0
            unknown_word.append(word)
        else:
            senses = target_word_to_senses[word]
            
            if sense not in senses:
                unkonwn_sense.append(sense)
                id = 0
            else:
                id = senses.index(sense)
        result.append(id)
            
    
    return result


def build_vocab(data, attribute='context'):
    """
    :param 
        data: list of dicts containing attribute 'context'
        attributes: The vocab you want to build(sense_list/ word_list)
    :return: 
        a dict with words as key and ids as value
    """
    counter = collections.Counter()
    for elem in data:
        counter.update(elem[attribute])

    # remove infrequent words
    min_freq = 5 #1
    filtered = [item for item in counter.items() if item[1] >= min_freq]

    count_pairs = sorted(filtered, key=lambda x: -x[1])
    words, _ = list(zip(*count_pairs))
    
    add_words = ['<pad>', '<eos>', '<unk>'] + list(words)
    word_to_id = dict(zip(add_words, range(len(add_words))))

    return word_to_id

def build_sense_vocab(data, attribute='target_sense', non_token=False):
    """
    :param 
        data: list of dicts containing attribute 'context'
        attributes: The vocab you want to build(sense_list/ word_list)
    :return: 
        a dict with words as key and ids as value
    """
    counter = collections.Counter()
    for elem in data:
        counter.update(elem['context'])
            
    sense_counter = collections.Counter()
    for elem in data:
        sense_counter.update(elem[attribute])

    # remove infrequent words
    min_freq = 3
    filtered = [item for item in counter.items() if item[1] >= min_freq]
    filtered_sesne = [item for item in sense_counter.items() if item[1] >= min_freq and item[0] != '<unk>']

    count_pairs = sorted(filtered, key=lambda x: -x[1])
    count_sense_pairs = sorted(filtered_sesne, key=lambda x: -x[1])
    words, _ = list(zip(*count_pairs))
    senses, _ = list(zip(*count_sense_pairs))
    
    # for coarse grain model, they have common parts
    senses = [i for i in senses if i not in words]
    
    if non_token:
        add_words = ['<pad>', '<eos>', '<unk>'] + list(words) + list(senses)
    else:
        add_words = ['<pad>', '<eos>', '<unk>'] + list(senses) 
    word_to_id = dict(zip(add_words, range(len(add_words))))

    return word_to_id

def convert_to_numeric(data, word_to_id, sense_to_id,
                       ignore_sense_not_in_train=True, mode=''):
    '''
    convert context(a list of token) into a list of id
    '''
    words_notin_vocab = []

#    with open('../tmp/pos_dic.pkl', 'rb') as f:
#        pos_to_id = pickle.load(f)

    all_data = []
#    target_tag_id = word_to_id['<target>']
#    instance_sensekey_not_in_train = []
    for insi, instance in enumerate(data):
        words = instance['context']
        poss = instance['poss']
        assert len(poss) == len(words)
        ctx_ints = []
#        pos_ints = []
        
        # In this step, we have already processed by get_correct_sense_index func
#        target_words = instance['context']
        
        for i, word in enumerate(words):
            if word in word_to_id:
                ctx_ints.append(word_to_id[word])
#                pos_ints.append(pos_to_id[poss[i]])
            elif len(word) > 0:
                ctx_ints.append(word_to_id['<unk>'])
#                pos_ints.append(pos_to_id['<unk>'])
                words_notin_vocab.append(word)

       

#        instance_id = instance['id']
#        targets_word = instance['target_word']
        target_sense = instance['target_sense']


        ins = Instance()
#        instance.id = instance_id
        ins.content = ctx_ints
#        instance.target_words = pos_ints[stop_idx]
        ins.target_pos = instance['target_word_pos_id_list']
        ins.sense_ids = target_sense

        all_data.append(ins)


    return all_data

def MLT_convert_to_numeric(data, word_to_id, sense_to_id, pos_to_id,lex_to_id,
                       ignore_sense_not_in_train=True, mode=''):
    '''
    convert context(a list of token) into a list of id
    '''
    words_notin_vocab = []
    words_notin_pos = []
    words_notin_lex = []

#    with open('../tmp/pos_dic.pkl', 'rb') as f:
#        pos_to_id = pickle.load(f)

    all_data = []
#    target_tag_id = word_to_id['<target>']
#    instance_sensekey_not_in_train = []
    for insi, instance in enumerate(data):
        words = instance['context']
        poss = instance['poss']
        lexes = instance['target_domain']
        
        assert len(poss) == len(words)
        ctx_ints = []
        pos_ints = []
        
        # In this step, we have already processed by get_correct_sense_index func
#        target_words = instance['context']
        
        for i, word in enumerate(words):
            if word in word_to_id:
                ctx_ints.append(word_to_id[word])
            elif len(word) > 0:
                ctx_ints.append(word_to_id['<unk>'])
                words_notin_vocab.append(word)
            
            if poss[i] in pos_to_id:
                pos_ints.append(pos_to_id[poss[i]])
            else:
                pos_ints.append(pos_to_id['<unk>'])
                words_notin_pos.append(word)
                

       

#        instance_id = instance['id']
#        targets_word = instance['target_word']
        target_sense = instance['target_sense']


        ins = Instance()
        ins.content = ctx_ints
        ins.pos_tag = pos_ints
        ins.lex_tag = lexes
        ins.target_pos = instance['target_word_pos_id_list']
        ins.sense_ids = target_sense

        all_data.append(ins)


    return all_data

'''
 x = {
            'target_word_pos_id_list': pos_id_list,
            'context': sent_list,
            'target_sense': sensekey_list,  # don't support multiple answers
            'target_word': target_word_list,
            'poss': pos_list,
     }
'''
def filter_word_and_sense(train_data, test_data = None, min_sense_freq=0, max_n_sense=40, attribute = "target_sense"):
    train_words = set()
    for elem in train_data:
        train_words = train_words | set(elem['target_word'])
#    print(train_words)
#    test_words = set()
#    if test_data:
#        for elem in test_data:
#            test_words.add(elem['target_word'])
#
#    target_words = train_words & test_words
    target_words = train_words
    counter = collections.Counter()
    for elem in train_data:
        for target_word, target_sense in zip(elem['target_word'],elem[attribute]):
            if  target_word in target_words:
                counter.update([target_sense])
    # remove infrequent sense
    filtered_sense = [item for item in counter.items() if item[1] >= min_sense_freq]
#    print(filtered_sense)
    count_pairs = sorted(filtered_sense, key=lambda x: -x[1])
    senses, _ = list(zip(*count_pairs))
    all_sense_to_id = dict(zip(senses, range(len(senses))))
    word_to_senses = {}
    for elem in train_data:
        for target_word,target_sense in zip(elem['target_word'],elem[attribute]):

            if target_sense in all_sense_to_id:
                if target_word not in word_to_senses:
                    word_to_senses.update({target_word: [target_sense]})
                else:
                    if target_sense not in word_to_senses[target_word]:
                        word_to_senses[target_word].append(target_sense)
#    print(word_to_senses)
    filtered_word_to_sense = {}
    for target_word, senses in word_to_senses.items():
        senses = sorted(senses, key=lambda s: all_sense_to_id[s])
        senses = senses[:max_n_sense]
        if len(senses) >= 1:  # must leave more than one sense
            np.random.shuffle(senses)  # shuffle senses to avoid MFS
            filtered_word_to_sense[target_word] = senses

    return filtered_word_to_sense

class Instance:
    def __init__(self):
        self.content = None
        self.target_pos = None
        self.sense_ids = None
        self.pos_tag = None
        self.lex_tag = None

def build_sense_ids(word_to_senses):

    words = list(word_to_senses.keys())
    target_word_to_id = dict(zip(words, range(len(words))))

    target_sense_to_id = [dict(zip(word_to_senses[word], range(len(word_to_senses[word])))) for word in words]

    n_senses_from_word_id = dict([(target_word_to_id[word], len(word_to_senses[word])) for word in words])
    return target_word_to_id, target_sense_to_id, n_senses_from_word_id, word_to_senses


def statistic_nums(data, target_word_to_senses,maximun_sense=10):
    '''
    parameter：
        data:batch data
        target_word_to_senses
    return:
        total_target_number
        maximum_sense_number: maximum sense number for a certain target word
        maximum_target_number: maximum target word number in a sentence
        maximum_setence_length: maximum content
    '''
    total_target_number = 0
    maximum_sense_number = 0
    maximum_target_number = 0
    maximum_setence_length = 0
    
    for ele in data:
        target_words = ele['target_word']
        content = ele['context']
        
        maximum_setence_length = max(maximum_setence_length, len(content))
        total_target_number += len(target_words)
        maximum_target_number = max(maximum_target_number, len(target_words))
        for word in target_words:
            if word in target_word_to_senses:
                maximum_sense_number = max(maximum_sense_number, len(target_word_to_senses[word]))
    
    return total_target_number, max(maximum_sense_number,maximun_sense), maximum_target_number, maximum_setence_length

def build_mask(dimention, is_target):
    '''
    parameter：
        dimention: 
            mask for bi_LSTM output: [batch_size, maximum_sentence_length, 2 * config.n_lstm_units]
            mask for logits: [batch_size, maximum_sentence_length, len(sense_vocab)]
    return:
        total_target_number
        maximum_sense_number: maximum sense number for a certain target word
        maximum_target_number: maximum target word number in a sentence
        maximum_setence_length: maximum content

    '''
    pass

def target_word_id_to_sense_id(target_word_to_senses,word_to_id, sense_to_id):
    result = {}
    for key,value in target_word_to_senses.items():
        ids = [sense_to_id[v] for v in value]
        result[word_to_id[key]] = ids
    return result

def MFS(train_data, test_data = None, min_sense_freq=0, max_n_sense=40, attributes = 'target_sense'):
    train_words = set()
    for elem in train_data:
        train_words = train_words | set(elem['target_word'])
#    print(train_words)
#    test_words = set()
#    if test_data:
#        for elem in test_data:
#            test_words.add(elem['target_word'])
#
#    target_words = train_words & test_words
    target_words = train_words
    counter = collections.Counter()
    for elem in train_data:
        for target_word, target_sense in zip(elem['target_word'],elem[attributes]):
            if  target_word in target_words:
                counter.update([target_sense])
    # remove infrequent sense
    filtered_sense = [item for item in counter.items() if item[1] >= min_sense_freq]
#    print(filtered_sense)
    count_pairs = sorted(filtered_sense, key=lambda x: -x[1])
    senses, _ = list(zip(*count_pairs))
    all_sense_to_id = dict(zip(senses, range(len(senses))))
    word_to_senses = {}
    for elem in train_data:
        for target_word,target_sense in zip(elem['target_word'],elem[attributes]):

            if target_sense in all_sense_to_id:
                if target_word not in word_to_senses:
                    word_to_senses.update({target_word: [target_sense]})
                else:
                    if target_sense not in word_to_senses[target_word]:
                        word_to_senses[target_word].append(target_sense)
    
    # sort the senses for a certain target_word
    for target_word, senses in word_to_senses.items():
        word_to_senses[target_word] = sorted(senses, key=lambda s: all_sense_to_id[s])
        
    return word_to_senses

def batch_generator(is_training, batch_size, data, pad_id, target_word_to_senses, word_to_id, sense_to_id,  pad_last_batch=False, elmo=False):
    '''
    This utility function can be used when you want to generate training batch or test batch
    
    parameter
        word_to_id: a dict mapping from input token to id(input)
        sense_to_id: a dict mapping from word token and sense to id(output)
        
    
    '''
    data_len = len(data)
    n_batches_float = data_len / float(batch_size)
    n_batches = int(math.ceil(n_batches_float)) if pad_last_batch else int(n_batches_float)
    
    

    if is_training:
        random.shuffle(data)

    for t in range(n_batches):
        batch = []
        batch = data[t * batch_size:(t + 1) * batch_size]

#        for ele in batch:
#            ele['target_sense'] = get_correct_sense_index(ele['target_word'], ele['target_sense'], target_word_to_senses)
        
        
        
        total_target_number, maximum_potential_sense_number, maximum_target_number, maximum_setence_length = statistic_nums(batch, target_word_to_senses, config.maximun_sense)
        output_sense_num = len(sense_to_id)
        
        batch = convert_to_numeric(batch, word_to_id, sense_to_id)
        maximum_setence_length = config.maximum_setence_length
        '''
        instance = Instance()
        instance.id = instance_id
        instance.content = ctx_ints
        instance.target_pos = instance['target_word_pos_id_list']
        instance.sense_ids = target_sense
        '''
        # context word
        sense_mask = np.zeros([batch_size,maximum_setence_length, output_sense_num], dtype=np.int32)
#        sentence_mask = np.zeros([batch_size,maximum_setence_length, config.n_lstm_units * 2], dtype=np.int32)
        
        context = np.zeros([batch_size, maximum_setence_length], dtype=np.int32)
        label = np.zeros([batch_size, maximum_setence_length, output_sense_num], dtype=np.int32)
        
        
        context.fill(pad_id)
        
        id_to_word = {}
        for key, value in word_to_id.items():
            id_to_word[value] = key
        # context pos
#        pfbs = np.zeros([batch_size, maximum_setence_length], dtype=np.int32)  # 0 is pad for pos, no need pad_id

        # x forward backward
        for j in range(len(batch)):
            ins_data = batch[j]
            content = ins_data.content
            pos = ins_data.target_pos
            target_sense = ins_data.sense_ids
            
            context[j][:len(ins_data.content)] = content
            
            for p, sense in zip(pos, target_sense):
                # j represents the j_th sample
                # p represents the p_th tokens
                if sense not in sense_to_id:
                    label[j][p][2] = 1 # 2 represents unknonw
                else:
                    label[j][p][sense_to_id[sense]] = 1
            
            for i in range(len(content)):
                
                
                random_labels = random.sample(range(1, output_sense_num), maximum_potential_sense_number)

                # the index of normal token should be consistent
                if i in pos:
                    if id_to_word[content[i]] in target_word_to_senses:
                        potentials = target_word_to_senses[id_to_word[content[i]]]
                    else:
                        potentials = [id_to_word[content[i]]]
                    potentials = [sense_to_id[value] if value in sense_to_id else 2 for value in potentials] #[sense_to_id[value] for value in potentials]
                    random_labels[:len(potentials)] = potentials
                    
                
                
                for k in random_labels:
                    sense_mask[j][i][k] = 1
                
                if i in pos:
                    continue
#                print('label', label.shape)
#                print('j',j, 'i',i,context[i])
#                print('sense_to_id[target_sense[i]', sense_to_id[target_sense[i]])
                #label[j][i][content[i]] = 0 #1
                
                
            
        origin_data = data[t * batch_size:(t + 1) * batch_size]
#        print('context', context)
        if elmo:
            batch_content = np.array([['<pad>']*maximum_setence_length]*batch_size)
            for index, tmp in enumerate(data[t * batch_size:(t + 1) * batch_size]):
                tmp_context = tmp['context']
                batch_content[index][:len(tmp_context)] = tmp_context
            yield ([' '.join(tmp) for tmp in batch_content],sense_mask ,label, origin_data, total_target_number, maximum_potential_sense_number, maximum_target_number, maximum_setence_length)
        else:
            yield (context,sense_mask ,label, origin_data, total_target_number, maximum_potential_sense_number, maximum_target_number, maximum_setence_length)

def MLT_batch_generator(is_training, batch_size, data, pad_id, target_word_to_senses, word_to_id, sense_to_id, pos_to_id=None, lex_to_id=None,  pad_last_batch=False, elmo=False):
    '''
    This utility function can be used when you want to generate training batch or test batch
    
    parameter
        word_to_id: a dict mapping from input token to id(input)
        sense_to_id: a dict mapping from word token and sense to id(output)
        
    
    '''
    data_len = len(data)
    n_batches_float = data_len / float(batch_size)
    n_batches = int(math.ceil(n_batches_float)) if pad_last_batch else int(n_batches_float)
    
    

    if is_training:
        random.shuffle(data)

    for t in range(n_batches):
        batch = []
        batch = data[t * batch_size:(t + 1) * batch_size]

#        for ele in batch:
#            ele['target_sense'] = get_correct_sense_index(ele['target_word'], ele['target_sense'], target_word_to_senses)
        

        total_target_number, maximum_potential_sense_number, maximum_target_number, maximum_setence_length = statistic_nums(batch, target_word_to_senses, config.maximun_sense)
        output_sense_num = len(sense_to_id)
        pos_num = len(pos_to_id)
        lex_num = len(lex_to_id)
        batch = MLT_convert_to_numeric(batch, word_to_id, sense_to_id, pos_to_id, lex_to_id)
        
        maximum_setence_length = config.maximum_setence_length
        '''
        instance = Instance()
        instance.id = instance_id
        instance.content = ctx_ints
        instance.target_pos = instance['target_word_pos_id_list']
        instance.sense_ids = target_sense
        '''
        # context word
        sense_mask = np.zeros([batch_size,maximum_setence_length, output_sense_num], dtype=np.int32)
#        sentence_mask = np.zeros([batch_size,maximum_setence_length, config.n_lstm_units * 2], dtype=np.int32)
        
        context = np.zeros([batch_size, maximum_setence_length], dtype=np.int32)
        label = np.zeros([batch_size, maximum_setence_length, output_sense_num], dtype=np.int32)
        poses = np.zeros([batch_size, maximum_setence_length, pos_num], dtype=np.int32)
        lexes = np.zeros([batch_size, maximum_setence_length, lex_num], dtype=np.int32)
        
        context.fill(pad_id)
        
        id_to_word = {}
        for key, value in word_to_id.items():
            id_to_word[value] = key
        # context pos
#        pfbs = np.zeros([batch_size, maximum_setence_length], dtype=np.int32)  # 0 is pad for pos, no need pad_id

        # x forward backward
        for j in range(batch_size):
            ins_data = batch[j]
            content = ins_data.content
            pos = ins_data.target_pos
            pos_tags = ins_data.pos_tag
            lex_tags = ins_data.lex_tag
            target_sense = ins_data.sense_ids
            
            context[j][:len(ins_data.content)] = content[:maximum_setence_length]
            
            for p, sense in zip(pos, target_sense):
#                if p >= maximum_setence_length:
#                    continue
                # j represents the j_th sample
                # p represents the p_th tokens
                if sense not in sense_to_id:
                    label[j][p][sense_to_id['<unk>']] = 1 # 2 represents unknonw
                else:
                    label[j][p][sense_to_id[sense]] = 1
                    
            for p, lex in zip(pos, lex_tags):
#                if p >= maximum_setence_length:
#                    continue
                # j represents the j_th sample
                # p represents the p_th tokens
                if lex not in lex_to_id:
                    lexes[j][p][lex_to_id['<unk>']] = 1 # 2 represents unknonw
                else:
                    lexes[j][p][lex_to_id[lex]] = 1
                    
            for i in range(len(content)):
                
                poses[j][i][pos_tags[i]] = 1 # 2 represents unknonw
                    
                random_labels = random.sample(range(1, output_sense_num), config.maximun_sense)

                # the index of normal token should be consistent
                if i in pos:
                    if id_to_word[content[i]] in target_word_to_senses:
                        potentials = target_word_to_senses[id_to_word[content[i]]]
                    else:
                        potentials = [id_to_word[content[i]]]
                    potentials = [sense_to_id[value] if value in sense_to_id else 2 for value in potentials] #[sense_to_id[value] for value in potentials]
                    random_labels[:len(potentials)] = potentials
                else:
                    random_labels[0] = content[i]
                
                
                for k in random_labels:
                    sense_mask[j][i][k] = 1
                
                if i in pos:
                    continue
#                print('label', label.shape)
#                print('j',j, 'i',i,context[i])
#                print('sense_to_id[target_sense[i]', sense_to_id[target_sense[i]])
                label[j][i][content[i]] = 1
                lexes[j][i][content[i]] = 1
                
                
            
        origin_data = data[t * batch_size:(t + 1) * batch_size]
        
        if elmo:
            batch_content = np.array([['<pad>']*maximum_setence_length]*batch_size)
            for index, tmp in enumerate(data[t * batch_size:(t + 1) * batch_size]):
                tmp_context = tmp['context']
                batch_content[index][:len(tmp_context)] = tmp_context
            yield ([' '.join(tmp) for tmp in batch_content], sense_mask ,label, poses, lexes, origin_data, total_target_number, maximum_potential_sense_number, maximum_target_number, maximum_setence_length)
        else:
            yield (context,sense_mask ,label, poses, lexes, origin_data, total_target_number, maximum_potential_sense_number, maximum_target_number, maximum_setence_length)

#        print('context', context)
       

def load_mapping(path):
    if path is None:
        print(u'No path exists')
        exit(-3)

    dicts = {}
    with open(path, 'r') as file:
        lines = file.readlines()
        for line in lines:
            line = line.strip()
            tokens = line.split()
            dicts[tokens[0]] = tokens[1]

    return dicts

def load_all_words_coarse_data(data_path, key_path=None, wn_to_babelnet_path=None, babelnet_to_domain_pth=None, is_training=False, word_to_id=None):
#    word_count_info = {}

    wn_to_babelnet = {}
    if wn_to_babelnet_path:
        babelnet_to_wn = load_mapping(wn_to_babelnet_path)
        for key, val in babelnet_to_wn.items():
            wn_to_babelnet[val] = key
    
    babelnet_to_domain = {}
    if babelnet_to_domain_pth:
        babelnet_to_domain = load_mapping(babelnet_to_domain_pth)
    context = et.iterparse(data_path, tag='sentence')

    id_to_sensekey = {}
    if key_path:
        id_to_sensekey = load_mapping(key_path)
    data = []
    poss = set()
    for event, elem in context:
        sent_list = []
        pos_list = []
        for child in elem:
            word = child.get('lemma').lower()
            if not word_to_id or word in word_to_id:
                sent_list.append(word)
            else:
                sent_list.append('<unk>')
            pos = child.get('pos')
            pos_list.append(pos)
            poss.add(pos)

        i = -1
        target_pos_id_list = []
        domain_list = []
        target_word_list = []
        sensekey_list = []
        target_id = []
        for child in elem:
            if child.tag == 'wf':
                i += 1
            elif child.tag == 'instance':
                i += 1
                id = child.get('id')
                lemma = child.get('lemma').lower()
                if '(' in lemma:
                    print( id)
                pos = child.get('pos')
                word = lemma + '#' + pos_dic[pos]
                
                target_id.append(id)
                if key_path:
                    sensekey = id_to_sensekey[id]
                    synset = wn.lemma_from_key(sensekey).synset()
                    synset_id = "wn:" + str(synset.offset()).zfill(8) + synset.pos()
                    if synset_id in wn_to_babelnet:
                        babelnet_id = wn_to_babelnet[synset_id]
                        if babelnet_id in babelnet_to_domain:
                            domain = babelnet_to_domain[babelnet_id]
                        else:
                            domain = '<unk>'
                    else:
                        domain = '<unk>'
                else:
                    domain = '<unk>'
                    sensekey = None
                if is_training:
                    if word_count_info[word][0] <= 1 or word_count_info[word][1] <= 1:
                        continue
                target_pos_id_list.append(i)
                domain_list.append(domain)
                sensekey_list.append(sensekey)
                # I only treat the lemma 
                target_word_list.append(word)
                
        #print(target_id) 
        assert len(target_pos_id_list) == len(domain_list)
        assert len(target_id) == len(domain_list)
        time = 0
        while len(sent_list) > 0:
            target_pos_id_list_tmp = []
            sent_list_tmp = sent_list[: config.maximum_setence_length]
            sensekey_list_tmp = []
            target_word_list_tmp = []
            domain_list_list_tmp = []
            target_id_list_tmp = []
            pos_list_tmp = pos_list[: config.maximum_setence_length]
            

    
            for index,value in enumerate(target_pos_id_list):
                if config.maximum_setence_length * time <= value < config.maximum_setence_length * (time + 1):
                    target_pos_id_list_tmp.append(value - config.maximum_setence_length * time)
                    sensekey_list_tmp.append(sensekey_list[index])
                    target_word_list_tmp.append(target_word_list[index])
                    domain_list_list_tmp.append(domain_list[index])
                    target_id_list_tmp.append(target_id[index])
                    
            
            pos_list = pos_list[config.maximum_setence_length * (time + 1):]
            sent_list = sent_list[config.maximum_setence_length * (time + 1):]
            time += 1
            assert len(target_pos_id_list_tmp) == len(sensekey_list_tmp)
            assert len(sent_list_tmp) == len(pos_list_tmp)

            x = {
                'target_word_pos_id_list': target_pos_id_list_tmp,
                'context': sent_list_tmp,
                'target_domain':domain_list_list_tmp,
                'target_sense': sensekey_list_tmp,  # don't support multiple answers
                'target_word': target_word_list_tmp,
                'poss': pos_list_tmp,
                'target_id': target_id_list_tmp
            }

            data.append(x)


    return data

def get_max_sentence_length(data):
    num = 0
    for element in data:
        num = max(num, len(element['context']))
    return num

def feed_dict(batch_data, is_training=True):
    context,sense_mask ,label,origin_data, total_target_number, maximum_potential_sense_number, maximum_target_number, maximum_setence_length = batch_data
    '''
    self.is_training = tf.placeholder(tf.bool, name='is_training')

    self.inputs = tf.placeholder(tf.int32, shape=[batch_size, n_step_f + n_step_b + 1], name='inputs')
    self.target_ids = tf.placeholder(tf.int32, shape=[batch_size, None], name='target_ids')
    self.sense_ids = tf.placeholder(tf.int32, shape=[batch_size, None], name='sense_ids')
    self.tot_n_senses = tf.placeholder(tf.int32, shape=[], name='tot_n_senses')
    '''
    feeds = {
        model.is_training: is_training,  # control keep_prob for dropout
        model.inputs: context,
        model.sense_mask: sense_mask,
        model.sense_ids: label,
#        model.tot_n_senses:total_target_number
    }
    return feeds
def save_obj(path, obj):
    with open(path, 'wb') as config_dictionary_file:
        pickle.dump(obj, config_dictionary_file)
        
def load_obj(path):
    with open(path, 'rb') as config_dictionary_file:
        # Step 3
        config_dictionary = pickle.load(config_dictionary_file)
     
    # After config_dictionary is read from file
    return config_dictionary#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 12 16:12:16 2019

@author: Kai Niu
"""


import lxml.etree as et
import math
import numpy as np
import collections
import re
import random
from bs4 import BeautifulSoup
from bs4 import NavigableString
import pickle
from nltk.corpus import wordnet as wn
from nltk.corpus.reader.wordnet import WordNetCorpusReader
from nltk.stem import WordNetLemmatizer
wordnet_lemmatizer = WordNetLemmatizer()  # download wordnet: import nltk; nltk.download("wordnet") in readme.txt


#_path = path.WSD_path()
#wn = WordNetCorpusReader(_path.WORDNET_PATH, '.*')
#print('wordnet version %s: %s' % (wn.get_version(), _path.WORDNET_PATH))

path_words_notin_vocab = '../tmp/words_notin_vocab_{}.txt'

pos_dic = {
    'ADJ': u'a',
    'ADV': u'r',
    'NOUN': u'n',
    'VERB': u'v', }

POS_LIST = pos_dic.values()  # ['a', 'r', 'n', 'v']

def load_all_words_data(data_path, key_path=None, is_training=False):
    word_count_info = {}

    id_to_sensekey = {}
    if key_path:
        for line in open(key_path).readlines():
            id = line.split()[0]
            sensekey = line.split()[1]  # multiple sense
            id_to_sensekey[id] = sensekey
#    print(id_to_sensekey)
    context = et.iterparse(data_path, tag='sentence')

    data = []
    poss = set()
    for event, elem in context:
        sent_list = []
        pos_list = []
        for child in elem:
            word = child.get('lemma').lower()
            sent_list.append(word)
            pos = child.get('pos')
            pos_list.append(pos)
            poss.add(pos)

        i = -1
        pos_id_list = []
        sensekey_list = []
        target_word_list = []
        for child in elem:
            if child.tag == 'wf':
                i += 1
            elif child.tag == 'instance':
                i += 1
                id = child.get('id')
                lemma = child.get('lemma').lower()
                if '(' in lemma:
                    print( id)
                pos = child.get('pos')
                word = lemma + '#' + pos_dic[pos]
                if key_path:
                    sensekey = id_to_sensekey[id]
                else:
                    sensekey = None
                if is_training:
                    if word_count_info[word][0] <= 1 or word_count_info[word][1] <= 1:
                        continue
                pos_id_list.append(i)
                sensekey_list.append(sensekey)
                # I only treat the lemma 
                target_word_list.append(lemma)
                
        assert len(pos_id_list) == len(sensekey_list)
        
        time = 0
        while len(sent_list) > 0:
            print(sent_list)
            pos_id_list_tmp = []
            sent_list_tmp = sent_list[: config.maximum_setence_length]
            sensekey_list_tmp = []
            target_word_list_tmp = []
            pos_list_tmp = pos_list[: config.maximum_setence_length]
            
            for index,value in enumerate(pos_id_list):
                if config.maximum_setence_length * time <= value < config.maximum_setence_length * (time + 1):
                    pos_id_list_tmp.append(value - config.maximum_setence_length * time)
                    sensekey_list_tmp.append(sensekey_list[index])
                    target_word_list_tmp.append(target_word_list[index])
                    
            
            pos_list = pos_list[config.maximum_setence_length * (time + 1):]
            sent_list = sent_list[config.maximum_setence_length * (time + 1):]
            time += 1
            
            assert len(pos_id_list_tmp) == len(sensekey_list_tmp)
            assert len(sent_list_tmp) == len(pos_list_tmp)
            x = {
                'target_word_pos_id_list': pos_id_list_tmp,
                'context': sent_list_tmp,
                'target_sense': sensekey_list_tmp,  # don't support multiple answers
                'target_word': target_word_list_tmp,
                'poss': pos_list_tmp,
            }

            data.append(x)


    return data


def get_correct_sense_index(target_words, target_senses, target_word_to_senses):
    """
    :param 
        target_words:
            a list containing all the 
        target_senses: 
            a list containing all the correct sense corresponding to the target word
        target_word_to_senses:
            a dict mapping from word to all the potential senses
        sense_vocab:
            a dict mapping from sense to sense_id
    :return: 
        a list containing the index for the target word in all potential senses
    """
    result = []
    unknown_word = []
    unkonwn_sense = []
    for word, sense in zip(target_words,target_senses):
        if word not in target_word_to_senses:
            # assume all the target_word
            id = 0
            unknown_word.append(word)
        else:
            senses = target_word_to_senses[word]
            
            if sense not in senses:
                unkonwn_sense.append(sense)
                id = 0
            else:
                id = senses.index(sense)
        result.append(id)
            
    
    return result


def build_vocab(data, attribute='context'):
    """
    :param 
        data: list of dicts containing attribute 'context'
        attributes: The vocab you want to build(sense_list/ word_list)
    :return: 
        a dict with words as key and ids as value
    """
    counter = collections.Counter()
    for elem in data:
        counter.update(elem[attribute])

    # remove infrequent words
    min_freq = 5 #1
    filtered = [item for item in counter.items() if item[1] >= min_freq]

    count_pairs = sorted(filtered, key=lambda x: -x[1])
    words, _ = list(zip(*count_pairs))
    
    add_words = ['<pad>', '<eos>', '<unk>'] + list(words)
    word_to_id = dict(zip(add_words, range(len(add_words))))

    return word_to_id

def build_sense_vocab(data, attribute='target_sense', non_token=False):
    """
    :param 
        data: list of dicts containing attribute 'context'
        attributes: The vocab you want to build(sense_list/ word_list)
    :return: 
        a dict with words as key and ids as value
    """
    counter = collections.Counter()
    for elem in data:
        counter.update(elem['context'])
            
    sense_counter = collections.Counter()
    for elem in data:
        sense_counter.update(elem[attribute])

    # remove infrequent words
    min_freq = 3
    filtered = [item for item in counter.items() if item[1] >= min_freq]
    filtered_sesne = [item for item in sense_counter.items() if item[1] >= min_freq and item[0] != '<unk>']

    count_pairs = sorted(filtered, key=lambda x: -x[1])
    count_sense_pairs = sorted(filtered_sesne, key=lambda x: -x[1])
    words, _ = list(zip(*count_pairs))
    senses, _ = list(zip(*count_sense_pairs))
    
    # for coarse grain model, they have common parts
    senses = [i for i in senses if i not in words]
    
    if non_token:
        add_words = ['<pad>', '<eos>', '<unk>'] + list(words) + list(senses)
    else:
        add_words = ['<pad>', '<eos>', '<unk>'] + list(senses) 
    word_to_id = dict(zip(add_words, range(len(add_words))))

    return word_to_id

def convert_to_numeric(data, word_to_id, sense_to_id,
                       ignore_sense_not_in_train=True, mode=''):
    '''
    convert context(a list of token) into a list of id
    '''
    words_notin_vocab = []

#    with open('../tmp/pos_dic.pkl', 'rb') as f:
#        pos_to_id = pickle.load(f)

    all_data = []
#    target_tag_id = word_to_id['<target>']
#    instance_sensekey_not_in_train = []
    for insi, instance in enumerate(data):
        words = instance['context']
        poss = instance['poss']
        assert len(poss) == len(words)
        ctx_ints = []
#        pos_ints = []
        
        # In this step, we have already processed by get_correct_sense_index func
#        target_words = instance['context']
        
        for i, word in enumerate(words):
            if word in word_to_id:
                ctx_ints.append(word_to_id[word])
#                pos_ints.append(pos_to_id[poss[i]])
            elif len(word) > 0:
                ctx_ints.append(word_to_id['<unk>'])
#                pos_ints.append(pos_to_id['<unk>'])
                words_notin_vocab.append(word)

       

#        instance_id = instance['id']
#        targets_word = instance['target_word']
        target_sense = instance['target_sense']


        ins = Instance()
#        instance.id = instance_id
        ins.content = ctx_ints
#        instance.target_words = pos_ints[stop_idx]
        ins.target_pos = instance['target_word_pos_id_list']
        ins.sense_ids = target_sense

        all_data.append(ins)


    return all_data

def MLT_convert_to_numeric(data, word_to_id, sense_to_id, pos_to_id,lex_to_id,
                       ignore_sense_not_in_train=True, mode=''):
    '''
    convert context(a list of token) into a list of id
    '''
    words_notin_vocab = []
    words_notin_pos = []
    words_notin_lex = []

#    with open('../tmp/pos_dic.pkl', 'rb') as f:
#        pos_to_id = pickle.load(f)

    all_data = []
#    target_tag_id = word_to_id['<target>']
#    instance_sensekey_not_in_train = []
    for insi, instance in enumerate(data):
        words = instance['context']
        poss = instance['poss']
        lexes = instance['target_domain']
        
        assert len(poss) == len(words)
        ctx_ints = []
        pos_ints = []
        
        # In this step, we have already processed by get_correct_sense_index func
#        target_words = instance['context']
        
        for i, word in enumerate(words):
            if word in word_to_id:
                ctx_ints.append(word_to_id[word])
            elif len(word) > 0:
                ctx_ints.append(word_to_id['<unk>'])
                words_notin_vocab.append(word)
            
            if poss[i] in pos_to_id:
                pos_ints.append(pos_to_id[poss[i]])
            else:
                pos_ints.append(pos_to_id['<unk>'])
                words_notin_pos.append(word)
                

       

#        instance_id = instance['id']
#        targets_word = instance['target_word']
        target_sense = instance['target_sense']


        ins = Instance()
        ins.content = ctx_ints
        ins.pos_tag = pos_ints
        ins.lex_tag = lexes
        ins.target_pos = instance['target_word_pos_id_list']
        ins.sense_ids = target_sense

        all_data.append(ins)


    return all_data

'''
 x = {
            'target_word_pos_id_list': pos_id_list,
            'context': sent_list,
            'target_sense': sensekey_list,  # don't support multiple answers
            'target_word': target_word_list,
            'poss': pos_list,
     }
'''
def filter_word_and_sense(train_data, test_data = None, min_sense_freq=0, max_n_sense=40, attribute = "target_sense"):
    train_words = set()
    for elem in train_data:
        train_words = train_words | set(elem['target_word'])
#    print(train_words)
#    test_words = set()
#    if test_data:
#        for elem in test_data:
#            test_words.add(elem['target_word'])
#
#    target_words = train_words & test_words
    target_words = train_words
    counter = collections.Counter()
    for elem in train_data:
        for target_word, target_sense in zip(elem['target_word'],elem[attribute]):
            if  target_word in target_words:
                counter.update([target_sense])
    # remove infrequent sense
    filtered_sense = [item for item in counter.items() if item[1] >= min_sense_freq]
#    print(filtered_sense)
    count_pairs = sorted(filtered_sense, key=lambda x: -x[1])
    senses, _ = list(zip(*count_pairs))
    all_sense_to_id = dict(zip(senses, range(len(senses))))
    word_to_senses = {}
    for elem in train_data:
        for target_word,target_sense in zip(elem['target_word'],elem[attribute]):

            if target_sense in all_sense_to_id:
                if target_word not in word_to_senses:
                    word_to_senses.update({target_word: [target_sense]})
                else:
                    if target_sense not in word_to_senses[target_word]:
                        word_to_senses[target_word].append(target_sense)
#    print(word_to_senses)
    filtered_word_to_sense = {}
    for target_word, senses in word_to_senses.items():
        senses = sorted(senses, key=lambda s: all_sense_to_id[s])
        senses = senses[:max_n_sense]
        if len(senses) >= 1:  # must leave more than one sense
            np.random.shuffle(senses)  # shuffle senses to avoid MFS
            filtered_word_to_sense[target_word] = senses

    return filtered_word_to_sense

class Instance:
    def __init__(self):
        self.content = None
        self.target_pos = None
        self.sense_ids = None
        self.pos_tag = None
        self.lex_tag = None

def build_sense_ids(word_to_senses):

    words = list(word_to_senses.keys())
    target_word_to_id = dict(zip(words, range(len(words))))

    target_sense_to_id = [dict(zip(word_to_senses[word], range(len(word_to_senses[word])))) for word in words]

    n_senses_from_word_id = dict([(target_word_to_id[word], len(word_to_senses[word])) for word in words])
    return target_word_to_id, target_sense_to_id, n_senses_from_word_id, word_to_senses


def statistic_nums(data, target_word_to_senses,maximun_sense=10):
    '''
    parameter：
        data:batch data
        target_word_to_senses
    return:
        total_target_number
        maximum_sense_number: maximum sense number for a certain target word
        maximum_target_number: maximum target word number in a sentence
        maximum_setence_length: maximum content
    '''
    total_target_number = 0
    maximum_sense_number = 0
    maximum_target_number = 0
    maximum_setence_length = 0
    
    for ele in data:
        target_words = ele['target_word']
        content = ele['context']
        
        maximum_setence_length = max(maximum_setence_length, len(content))
        total_target_number += len(target_words)
        maximum_target_number = max(maximum_target_number, len(target_words))
        for word in target_words:
            if word in target_word_to_senses:
                maximum_sense_number = max(maximum_sense_number, len(target_word_to_senses[word]))
    
    return total_target_number, max(maximum_sense_number,maximun_sense), maximum_target_number, maximum_setence_length

def build_mask(dimention, is_target):
    '''
    parameter：
        dimention: 
            mask for bi_LSTM output: [batch_size, maximum_sentence_length, 2 * config.n_lstm_units]
            mask for logits: [batch_size, maximum_sentence_length, len(sense_vocab)]
    return:
        total_target_number
        maximum_sense_number: maximum sense number for a certain target word
        maximum_target_number: maximum target word number in a sentence
        maximum_setence_length: maximum content

    '''
    pass

def target_word_id_to_sense_id(target_word_to_senses,word_to_id, sense_to_id):
    result = {}
    for key,value in target_word_to_senses.items():
        ids = [sense_to_id[v] for v in value]
        result[word_to_id[key]] = ids
    return result

def MFS(train_data, test_data = None, min_sense_freq=0, max_n_sense=40, attributes = 'target_sense'):
    train_words = set()
    for elem in train_data:
        train_words = train_words | set(elem['target_word'])
#    print(train_words)
#    test_words = set()
#    if test_data:
#        for elem in test_data:
#            test_words.add(elem['target_word'])
#
#    target_words = train_words & test_words
    target_words = train_words
    counter = collections.Counter()
    for elem in train_data:
        for target_word, target_sense in zip(elem['target_word'],elem[attributes]):
            if  target_word in target_words:
                counter.update([target_sense])
    # remove infrequent sense
    filtered_sense = [item for item in counter.items() if item[1] >= min_sense_freq]
#    print(filtered_sense)
    count_pairs = sorted(filtered_sense, key=lambda x: -x[1])
    senses, _ = list(zip(*count_pairs))
    all_sense_to_id = dict(zip(senses, range(len(senses))))
    word_to_senses = {}
    for elem in train_data:
        for target_word,target_sense in zip(elem['target_word'],elem[attributes]):

            if target_sense in all_sense_to_id:
                if target_word not in word_to_senses:
                    word_to_senses.update({target_word: [target_sense]})
                else:
                    if target_sense not in word_to_senses[target_word]:
                        word_to_senses[target_word].append(target_sense)
    
    # sort the senses for a certain target_word
    for target_word, senses in word_to_senses.items():
        word_to_senses[target_word] = sorted(senses, key=lambda s: all_sense_to_id[s])
        
    return word_to_senses

def batch_generator(is_training, batch_size, data, pad_id, target_word_to_senses, word_to_id, sense_to_id,  pad_last_batch=False, elmo=False):
    '''
    This utility function can be used when you want to generate training batch or test batch
    
    parameter
        word_to_id: a dict mapping from input token to id(input)
        sense_to_id: a dict mapping from word token and sense to id(output)
        
    
    '''
    data_len = len(data)
    n_batches_float = data_len / float(batch_size)
    n_batches = int(math.ceil(n_batches_float)) if pad_last_batch else int(n_batches_float)
    
    

    if is_training:
        random.shuffle(data)

    for t in range(n_batches):
        batch = []
        batch = data[t * batch_size:(t + 1) * batch_size]

#        for ele in batch:
#            ele['target_sense'] = get_correct_sense_index(ele['target_word'], ele['target_sense'], target_word_to_senses)
        
        
        
        total_target_number, maximum_potential_sense_number, maximum_target_number, maximum_setence_length = statistic_nums(batch, target_word_to_senses, config.maximun_sense)
        output_sense_num = len(sense_to_id)
        
        batch = convert_to_numeric(batch, word_to_id, sense_to_id)
        maximum_setence_length = config.maximum_setence_length
        '''
        instance = Instance()
        instance.id = instance_id
        instance.content = ctx_ints
        instance.target_pos = instance['target_word_pos_id_list']
        instance.sense_ids = target_sense
        '''
        # context word
        sense_mask = np.zeros([batch_size,maximum_setence_length, output_sense_num], dtype=np.int32)
#        sentence_mask = np.zeros([batch_size,maximum_setence_length, config.n_lstm_units * 2], dtype=np.int32)
        
        context = np.zeros([batch_size, maximum_setence_length], dtype=np.int32)
        label = np.zeros([batch_size, maximum_setence_length, output_sense_num], dtype=np.int32)
        
        
        context.fill(pad_id)
        
        id_to_word = {}
        for key, value in word_to_id.items():
            id_to_word[value] = key
        # context pos
#        pfbs = np.zeros([batch_size, maximum_setence_length], dtype=np.int32)  # 0 is pad for pos, no need pad_id

        # x forward backward
        for j in range(len(batch)):
            ins_data = batch[j]
            content = ins_data.content
            pos = ins_data.target_pos
            target_sense = ins_data.sense_ids
            
            context[j][:len(ins_data.content)] = content
            
            for p, sense in zip(pos, target_sense):
                # j represents the j_th sample
                # p represents the p_th tokens
                if sense not in sense_to_id:
                    label[j][p][2] = 1 # 2 represents unknonw
                else:
                    label[j][p][sense_to_id[sense]] = 1
            
            for i in range(len(content)):
                
                
                random_labels = random.sample(range(1, output_sense_num), maximum_potential_sense_number)

                # the index of normal token should be consistent
                if i in pos:
                    if id_to_word[content[i]] in target_word_to_senses:
                        potentials = target_word_to_senses[id_to_word[content[i]]]
                    else:
                        potentials = [id_to_word[content[i]]]
                    potentials = [sense_to_id[value] if value in sense_to_id else 2 for value in potentials] #[sense_to_id[value] for value in potentials]
                    random_labels[:len(potentials)] = potentials
                    
                
                
                for k in random_labels:
                    sense_mask[j][i][k] = 1
                
                if i in pos:
                    continue
#                print('label', label.shape)
#                print('j',j, 'i',i,context[i])
#                print('sense_to_id[target_sense[i]', sense_to_id[target_sense[i]])
                #label[j][i][content[i]] = 0 #1
                
                
            
        origin_data = data[t * batch_size:(t + 1) * batch_size]
#        print('context', context)
        if elmo:
            batch_content = np.array([['<pad>']*maximum_setence_length]*batch_size)
            for index, tmp in enumerate(data[t * batch_size:(t + 1) * batch_size]):
                tmp_context = tmp['context']
                batch_content[index][:len(tmp_context)] = tmp_context
            yield ([' '.join(tmp) for tmp in batch_content],sense_mask ,label, origin_data, total_target_number, maximum_potential_sense_number, maximum_target_number, maximum_setence_length)
        else:
            yield (context,sense_mask ,label, origin_data, total_target_number, maximum_potential_sense_number, maximum_target_number, maximum_setence_length)

def MLT_batch_generator(is_training, batch_size, data, pad_id, target_word_to_senses, word_to_id, sense_to_id, pos_to_id=None, lex_to_id=None,  pad_last_batch=False, elmo=False):
    '''
    This utility function can be used when you want to generate training batch or test batch
    
    parameter
        word_to_id: a dict mapping from input token to id(input)
        sense_to_id: a dict mapping from word token and sense to id(output)
        
    
    '''
    data_len = len(data)
    n_batches_float = data_len / float(batch_size)
    n_batches = int(math.ceil(n_batches_float)) if pad_last_batch else int(n_batches_float)
    
    

    if is_training:
        random.shuffle(data)

    for t in range(n_batches):
        batch = []
        batch = data[t * batch_size:(t + 1) * batch_size]

#        for ele in batch:
#            ele['target_sense'] = get_correct_sense_index(ele['target_word'], ele['target_sense'], target_word_to_senses)
        

        total_target_number, maximum_potential_sense_number, maximum_target_number, maximum_setence_length = statistic_nums(batch, target_word_to_senses, config.maximun_sense)
        output_sense_num = len(sense_to_id)
        pos_num = len(pos_to_id)
        lex_num = len(lex_to_id)
        batch = MLT_convert_to_numeric(batch, word_to_id, sense_to_id, pos_to_id, lex_to_id)
        
        maximum_setence_length = config.maximum_setence_length
        '''
        instance = Instance()
        instance.id = instance_id
        instance.content = ctx_ints
        instance.target_pos = instance['target_word_pos_id_list']
        instance.sense_ids = target_sense
        '''
        # context word
        sense_mask = np.zeros([batch_size,maximum_setence_length, output_sense_num], dtype=np.int32)
#        sentence_mask = np.zeros([batch_size,maximum_setence_length, config.n_lstm_units * 2], dtype=np.int32)
        
        context = np.zeros([batch_size, maximum_setence_length], dtype=np.int32)
        label = np.zeros([batch_size, maximum_setence_length, output_sense_num], dtype=np.int32)
        poses = np.zeros([batch_size, maximum_setence_length, pos_num], dtype=np.int32)
        lexes = np.zeros([batch_size, maximum_setence_length, lex_num], dtype=np.int32)
        
        context.fill(pad_id)
        
        id_to_word = {}
        for key, value in word_to_id.items():
            id_to_word[value] = key
        # context pos
#        pfbs = np.zeros([batch_size, maximum_setence_length], dtype=np.int32)  # 0 is pad for pos, no need pad_id

        # x forward backward
        for j in range(batch_size):
            ins_data = batch[j]
            content = ins_data.content
            pos = ins_data.target_pos
            pos_tags = ins_data.pos_tag
            lex_tags = ins_data.lex_tag
            target_sense = ins_data.sense_ids
            
            context[j][:len(ins_data.content)] = content[:maximum_setence_length]
            
            for p, sense in zip(pos, target_sense):
#                if p >= maximum_setence_length:
#                    continue
                # j represents the j_th sample
                # p represents the p_th tokens
                if sense not in sense_to_id:
                    label[j][p][sense_to_id['<unk>']] = 1 # 2 represents unknonw
                else:
                    label[j][p][sense_to_id[sense]] = 1
                    
            for p, lex in zip(pos, lex_tags):
#                if p >= maximum_setence_length:
#                    continue
                # j represents the j_th sample
                # p represents the p_th tokens
                if lex not in lex_to_id:
                    lexes[j][p][lex_to_id['<unk>']] = 1 # 2 represents unknonw
                else:
                    lexes[j][p][lex_to_id[lex]] = 1
                    
            for i in range(len(content)):
                
                poses[j][i][pos_tags[i]] = 1 # 2 represents unknonw
                    
                random_labels = random.sample(range(1, output_sense_num), config.maximun_sense)

                # the index of normal token should be consistent
                if i in pos:
                    if id_to_word[content[i]] in target_word_to_senses:
                        potentials = target_word_to_senses[id_to_word[content[i]]]
                    else:
                        potentials = [id_to_word[content[i]]]
                    potentials = [sense_to_id[value] if value in sense_to_id else 2 for value in potentials] #[sense_to_id[value] for value in potentials]
                    random_labels[:len(potentials)] = potentials
                else:
                    random_labels[0] = content[i]
                
                
                for k in random_labels:
                    sense_mask[j][i][k] = 1
                
                if i in pos:
                    continue
#                print('label', label.shape)
#                print('j',j, 'i',i,context[i])
#                print('sense_to_id[target_sense[i]', sense_to_id[target_sense[i]])
                label[j][i][content[i]] = 1
                lexes[j][i][content[i]] = 1
                
                
            
        origin_data = data[t * batch_size:(t + 1) * batch_size]
        
        if elmo:
            batch_content = np.array([['<pad>']*maximum_setence_length]*batch_size)
            for index, tmp in enumerate(data[t * batch_size:(t + 1) * batch_size]):
                tmp_context = tmp['context']
                batch_content[index][:len(tmp_context)] = tmp_context
            yield ([' '.join(tmp) for tmp in batch_content], sense_mask ,label, poses, lexes, origin_data, total_target_number, maximum_potential_sense_number, maximum_target_number, maximum_setence_length)
        else:
            yield (context,sense_mask ,label, poses, lexes, origin_data, total_target_number, maximum_potential_sense_number, maximum_target_number, maximum_setence_length)

#        print('context', context)
       

def load_mapping(path):
    if path is None:
        print(u'No path exists')
        exit(-3)

    dicts = {}
    with open(path, 'r') as file:
        lines = file.readlines()
        for line in lines:
            line = line.strip()
            tokens = line.split()
            dicts[tokens[0]] = tokens[1]

    return dicts

def load_all_words_coarse_data(data_path, key_path=None, wn_to_babelnet_path=None, babelnet_to_domain_pth=None, is_training=False, word_to_id=None, coarse=False):
#    word_count_info = {}

    wn_to_babelnet = {}
    if wn_to_babelnet_path:
        babelnet_to_wn = load_mapping(wn_to_babelnet_path)
        for key, val in babelnet_to_wn.items():
            wn_to_babelnet[val] = key
    
    babelnet_to_domain = {}
    if babelnet_to_domain_pth:
        babelnet_to_domain = load_mapping(babelnet_to_domain_pth)
    context = et.iterparse(data_path, tag='sentence')

    id_to_sensekey = {}
    if key_path:
        id_to_sensekey = load_mapping(key_path)
    data = []
    poss = set()
    for event, elem in context:
        sent_list = []
        pos_list = []
        for child in elem:
            word = child.get('lemma').lower()
            if not word_to_id or word in word_to_id:
                sent_list.append(word)
            else:
                sent_list.append('<unk>')
            pos = child.get('pos')
            pos_list.append(pos)
            poss.add(pos)

        i = -1
        target_pos_id_list = []
        domain_list = []
        target_word_list = []
        sensekey_list = []
        target_id = []
        for child in elem:
            if child.tag == 'wf':
                i += 1
            elif child.tag == 'instance':
                i += 1
                id = child.get('id')
                lemma = child.get('lemma').lower()
                if '(' in lemma:
                    print( id)
                pos = child.get('pos')
                word = lemma + '#' + pos_dic[pos]
                
                target_id.append(id)
                if key_path:
                    sensekey = id_to_sensekey[id]
                    synset = wn.lemma_from_key(sensekey).synset()
                    synset_id = "wn:" + str(synset.offset()).zfill(8) + synset.pos()
                    if synset_id in wn_to_babelnet:
                        babelnet_id = wn_to_babelnet[synset_id]
                        if babelnet_id in babelnet_to_domain:
                            domain = babelnet_to_domain[babelnet_id]
                        else:
                            domain = '<unk>'
                    else:
                        domain = '<unk>'
                else:
                    domain = '<unk>'
                    sensekey = None
                if is_training:
                    if word_count_info[word][0] <= 1 or word_count_info[word][1] <= 1:
                        continue
                target_pos_id_list.append(i)
                domain_list.append(domain)
                sensekey_list.append(sensekey)
                # I only treat the lemma 
                target_word_list.append(lemma)
                
        #print(target_id) 
        assert len(target_pos_id_list) == len(domain_list)
        assert len(target_id) == len(domain_list)
        time = 0
        while len(sent_list) > 0:
            target_pos_id_list_tmp = []
            sent_list_tmp = sent_list[: config.maximum_setence_length]
            sensekey_list_tmp = []
            target_word_list_tmp = []
            domain_list_list_tmp = []
            target_id_list_tmp = []
            pos_list_tmp = pos_list[: config.maximum_setence_length]
            

    
            for index,value in enumerate(target_pos_id_list):
                if config.maximum_setence_length * time <= value < config.maximum_setence_length * (time + 1):
                    target_pos_id_list_tmp.append(value - config.maximum_setence_length * time)
                    sensekey_list_tmp.append(sensekey_list[0])
                    target_word_list_tmp.append(target_word_list[0])
                    domain_list_list_tmp.append(domain_list[0])
                    target_id_list_tmp.append(target_id[0])
                    
                    sensekey_list.pop(0)
                    target_word_list.pop(0)
                    domain_list.pop(0)
                    target_id.pop(0)
                    
            
            pos_list = pos_list[config.maximum_setence_length * (time + 1):]
            sent_list = sent_list[config.maximum_setence_length * (time + 1):]
            time += 1
            assert len(target_pos_id_list_tmp) == len(sensekey_list_tmp)
            assert len(sent_list_tmp) == len(pos_list_tmp)
            if coarse:
                x = {
                    'target_word_pos_id_list': target_pos_id_list_tmp,
                    'context': sent_list_tmp,
                    'target_domain':domain_list_list_tmp,
                    'target_sense': domain_list_list_tmp,  # don't support multiple answers
                    'target_word': target_word_list_tmp,
                    'poss': pos_list_tmp,
                    'target_id': target_id_list_tmp
                }
            else:
                x = {
                    'target_word_pos_id_list': target_pos_id_list_tmp,
                    'context': sent_list_tmp,
                    'target_domain':domain_list_list_tmp,
                    'target_sense': sensekey_list_tmp,  # don't support multiple answers
                    'target_word': target_word_list_tmp,
                    'poss': pos_list_tmp,
                    'target_id': target_id_list_tmp
                }

            data.append(x)


    return data

def get_max_sentence_length(data):
    num = 0
    for element in data:
        num = max(num, len(element['context']))
    return num

def save_obj(path, obj):
    with open(path, 'wb') as config_dictionary_file:
        pickle.dump(obj, config_dictionary_file)
        
def load_obj(path):
    with open(path, 'rb') as config_dictionary_file:
        # Step 3
        config_dictionary = pickle.load(config_dictionary_file)
     
    # After config_dictionary is read from file
    return config_dictionary

