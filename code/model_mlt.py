#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 12 16:12:16 2019

@author: Kai Niu
"""

import tensorflow as tf
import numpy as np

rnn = tf.contrib.rnn


class Model:
    def __init__(self, config, n_senses_from_target_id, init_word_vecs=None):
        self.name = 'GAS'

        batch_size = config.batch_size
#        n_step_f = config.n_step_f
#        n_step_b = config.n_step_b

        embedding_size = config.embedding_size
        n_units = config.n_lstm_units
        
        sense_num = config.sense_num
        pos_num = config.pos_num
        lex_num = config.lex_num
        maximum_setence_length = config.maximum_setence_length
        forget_bias = config.forget_bias
        keep_prob = config.keep_prob
        lambda_l2_reg = config.lambda_l2_reg
        momentum = config.momentum
        max_grad_norm = config.max_grad_norm


        if config.concat_target_gloss:
            n_units /= 2


        lr_start = config.lr_start  # 0.2
        lr_decay_factor = 0.96
        lr_min = 0.01

        self.is_training = tf.placeholder(tf.bool, name='is_training')

        self.inputs = tf.placeholder(tf.int32, shape=[batch_size, maximum_setence_length], name='inputs')
        self.sense_mask = tf.placeholder(tf.float32, shape=[batch_size, maximum_setence_length, None], name='sense_mask')
        self.sense_ids = tf.placeholder(tf.float32, shape=[batch_size, maximum_setence_length, None], name='sense_ids')
        self.pos = tf.placeholder(tf.float32, shape=[batch_size, maximum_setence_length, None], name='sense_ids')
        self.lex = tf.placeholder(tf.float32, shape=[batch_size, maximum_setence_length, None], name='sense_ids')
#        self.sense_ids = tf.placeholder(tf.int32, shape=[batch_size, maximum_setence_length, None], name='sense_ids')

#        self.predictions = tf.Variable(tf.zeros([batch_size], dtype=tf.int32), trainable=False)
#        self.correct = tf.Variable(tf.zeros([batch_size], dtype=tf.int32), trainable=False)
        self.global_step = global_step = tf.Variable(0, trainable=False)

        global_initializer = tf.random_uniform_initializer(-0.1, 0.1)
        lstm_initializer = tf.orthogonal_initializer()

        with tf.device('/cpu:0'):
            with tf.variable_scope('word_emb',reuse=tf.AUTO_REUSE):
                if config.use_pre_trained_embedding:
                    word_embeddings = tf.get_variable('word_embeddings', initializer=init_word_vecs, trainable=False)
                else:
                    word_embeddings = tf.get_variable('word_embeddings',
                                                      shape=[config.vocab_size, embedding_size],
                                                  initializer=tf.random_uniform_initializer(-0.1, 0.1), trainable=True)

       
        with tf.variable_scope('target_params',reuse=tf.AUTO_REUSE):
            self.W_targets = tf.get_variable('W_targets', [4*config.n_lstm_units, sense_num], dtype=tf.float32,
                                        initializer=tf.contrib.layers.xavier_initializer(
                                                uniform=True, seed=42, dtype=tf.dtypes.float32))
            
            self.b_target = tf.get_variable('b_target', [sense_num], dtype=tf.float32,
                                       initializer=global_initializer)
        
        with tf.variable_scope('pos_tag',reuse=tf.AUTO_REUSE):
            self.W_pos = tf.get_variable('W_pos', [4*config.n_lstm_units, pos_num], dtype=tf.float32,
                                        initializer=tf.contrib.layers.xavier_initializer(
                                                uniform=True, seed=42, dtype=tf.dtypes.float32))
            
            self.b_pos = tf.get_variable('b_pos', [pos_num], dtype=tf.float32,
                                       initializer=global_initializer)
        
        with tf.variable_scope('lex_tag',reuse=tf.AUTO_REUSE):
            self.W_lex = tf.get_variable('W_lex', [4*config.n_lstm_units, lex_num], dtype=tf.float32,
                                        initializer=tf.contrib.layers.xavier_initializer(
                                                uniform=True, seed=42, dtype=tf.dtypes.float32))
            self.b_lex = tf.get_variable('b_lex', [lex_num], dtype=tf.float32,
                                       initializer=global_initializer)
            
        self.keep_prob = keep_prob = tf.cond(tf.equal(self.is_training, tf.constant(True)),
                                             lambda: tf.constant(config.keep_prob),
                                             lambda: tf.constant(1.0))    # val or test: 1.0 means no dropout

        def lstm_cell(num_units):
            cell = rnn.LSTMCell(num_units, initializer=lstm_initializer, forget_bias=forget_bias)
            if tf.__version__ == '1.2.0' and config.state_dropout:
                cell = rnn.DropoutWrapper(cell, output_keep_prob=keep_prob, state_keep_prob=keep_prob)
            else:
                cell = rnn.DropoutWrapper(cell, output_keep_prob=keep_prob)
            return cell

        with tf.variable_scope('sentence', initializer=lstm_initializer):
            s_cell_fw = lstm_cell(n_units)  # forward
            s_cell_bw = lstm_cell(n_units)  # backward
            inputs_s = tf.nn.embedding_lookup(word_embeddings, self.inputs)  # [batch_size, n_step, dim]
            inputs_s = tf.nn.dropout(inputs_s, keep_prob)
            s_outputs, s_final_state = tf.nn.bidirectional_dynamic_rnn(s_cell_fw,
                                                                       s_cell_bw,
                                                                       inputs_s,
                                                                       dtype=tf.float32,
                                                                       time_major=False)
            
            s_outputs_fw = s_outputs[0]
            s_outputs_bw = s_outputs[1]  # [batch_size, n_step, n_units]
            
            rnn_outputs = tf.concat(axis=2, values=[s_outputs_fw, s_outputs_bw]) #[batch_size, n_step, 2*n_units]
            rnn_outputs_shape = rnn_outputs.shape
            print(rnn_outputs_shape)
        
        
        with tf.name_scope("attention"):
            self.attention_score = tf.nn.softmax(tf.contrib.slim.fully_connected(rnn_outputs, 1))
#            print(self.attention_score)
            c = self.c = tf.squeeze(
                tf.matmul(tf.transpose(rnn_outputs, perm=[0, 2, 1]), self.attention_score),
                axis=-1)
            
        #c = tf.expand_dims(c,1)
        #extend_c = tf.broadcast_to(c, [tf.cast(self.inputs[1],dtype=tf.int32), tf.cast(c.shape[0],dtype=tf.int32),tf.cast( c.shape[1],dtype=tf.int32),tf.cast( c.shape[2],dtype=tf.int32)])
        #extend_c = tf.tile(c, tf.cast(self.inputs[1],dtype=tf.int32))
        #c = tf.concat(extend_c ,axis=1)
        #print('c.shape', c.shape)
        c = tf.expand_dims(c,1)
        c = tf.concat([c]*self.inputs.shape[1],axis=1)
        
        
        merged_out = tf.concat([rnn_outputs,c], axis=2)
        print(merged_out)
        output = self.output = tf.reshape(merged_out,[-1, rnn_outputs.shape[-1]*2])
#        print(output) 
#        self.output = output = tf.reshape(output, [-1, 2 * n_units]) # batch_size * n_step, 2*n_units
        
        self.logits = logits  = tf.matmul(output, self.W_targets) + self.b_target # batch_size * n_step, num_tags
        pos = tf.matmul(output, self.W_pos) + self.b_pos
        lex = tf.matmul(output, self.W_lex) + self.b_lex
        
#        logits = tf.reshape(pred, [-1, shape[1], sense_num])
        labels = tf.reshape(self.sense_ids,[-1,sense_num])
        pos_labels = tf.reshape(self.pos,[-1,pos_num])
        lex_labels = tf.reshape(self.lex,[-1,lex_num])
        
        
        sense_mask = tf.reshape(self.sense_mask,[-1, logits.shape[-1]])
        
        tf.print('self.sense_mask.shape', self.sense_mask.shape)
        pre = tf.math.multiply(sense_mask , logits)
        self.predicted_sense = tf.argmax(pre, 1, name='prediction')
        self.predicted_sense = tf.cast(self.predicted_sense, tf.int32)
#        print('logits.shape', logits.shape)
        # prediction——a list (n_step, n_units)
#        prediction = tf.split(logits, batch_size, 0)
        self.score = tf.reduce_sum(labels * (-tf.log(
                tf.clip_by_value(tf.math.divide(tf.exp(logits), tf.reduce_sum(tf.exp(logits), -1,keep_dims=True)),1e-10,20))))
        
        self.lex_loss = tf.reduce_sum(lex_labels * (-tf.log(
                tf.clip_by_value(tf.math.divide(tf.exp(lex), tf.reduce_sum(tf.exp(lex), -1,keep_dims=True)),1e-10,20))))
        
        self.pos_loss = tf.reduce_sum(pos_labels * (-tf.log(
                tf.clip_by_value(tf.math.divide(tf.exp(pos), tf.reduce_sum(tf.exp(pos), -1,keep_dims=True)),1e-10,20))))
        
        #self.pos_loss = tf.reduce_sum(tf.nn.softmax_cross_entropy_with_logits(labels = pos_labels, logits=pos))
        #self.lex_loss = tf.reduce_sum(tf.nn.softmax_cross_entropy_with_logits(labels = lex_labels, logits=lex))
        self.loss_op = 0.5 * self.score + 0.25 * self.pos_loss + 0.25 * self.lex_loss
       

        self.loss_op = tf.div(self.loss_op, batch_size * maximum_setence_length * 0.5)
#        self.accuracy_op = correct / total

        # Summaries
        tf.summary.scalar('loss', self.loss_op)
#        tf.summary.scalar('accuracy', self.accuracy_op)
        self.summary_op = tf.summary.merge_all()

        print( 'TRAINABLE VARIABLES')
        tvars = tf.trainable_variables()

        # Weight Penalty
        if lambda_l2_reg:
            print( 'USING L2 regularization')
            w_cost = tf.constant(0.0)
            n_w = tf.constant(0.0)
            for tvar in tvars:
                if 'lstm' in tvar.name or 'memory' in tvar.name:
                    print(tvar.name)
                    w_cost += tf.nn.l2_loss(tvar)
                    n_w += tf.to_float(tf.size(tvar))
            self.loss_op += lambda_l2_reg * w_cost / n_w

        # Update Parameters
        self.grads, _ = tf.clip_by_global_norm(tf.gradients(self.loss_op, tvars), max_grad_norm)
        self.lr = tf.math.maximum(tf.maximum(lr_min, tf.train.exponential_decay(lr_start, global_step, 100, lr_decay_factor)), config.minimum_lr)
        optimizer = tf.train.MomentumOptimizer(self.lr, momentum)
        self.train_op = optimizer.apply_gradients(zip(self.grads, tvars), global_step=global_step)

        # Both ok
        # self.lr = tf.Variable(lr_start, trainable=False)
        # optimizer = tf.train.AdamOptimizer(self.lr)
        # self.train_op = optimizer.minimize(self.loss_op, global_step=global_step, var_list=tvars)
