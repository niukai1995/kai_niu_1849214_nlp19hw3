#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 14 07:54:01 2019

@author: mark
"""

Training_data_path = '../resources/WSD_Training_Corpora/SemCor/semcor.data.xml'
Train_Sense_key = '../resources/WSD_Training_Corpora/SemCor/semcor.gold.key.txt'
#Dev_data_path = '../resources/WSD_Testing_Corpora/semeval2007/semeval2007.data.xml'
#Dev_Sense_key = '../resources/WSD_Testing_Corpora/semeval2007/semeval2007.gold.key.txt'
babelnet2lexnames = '../resources/babelnet2lexnames.tsv'
babelnet2wndomains = '../resources/babelnet2wndomains.tsv'
babelnet2wordnet = '../resources/babelnet2wordnet.tsv'

mfs_path = '../resources/mfs.dictionary'
mfs_domain_path = '../resources/mfs_domain.dictionary'
mfs_lex_path = '../resources/mfs_lex.dictionary'

word_to_id_path = '../resources/word_to_id.dictionary'

sense_to_id_path = '../resources/sense_to_id.dictionary'
sense_to_id_domain_path = '../resources/sense_to_id_domain.dictionary'
sense_to_id_lex_path = '../resources/sense_to_id_lex.dictionary'

Dev_data_path = '../resources/WSD_Unified_Evaluation_Datasets/ALL/ALL.data.xml'
Dev_Sense_key = '../resources/WSD_Unified_Evaluation_Datasets/ALL/ALL.gold.key.txt'
resources_path = '../resources/model/Basic_model_elmo_0.9_1024_0.5_0.8_5_3_version2/'
domain_model_path = '../resources/Basic_model_elmo_0.9_1024_0.5_0.8_5_3_fix_bug_in_preprocessing_coarse'

target_word_to_senses_path = '../resources/target_word_to_senses.dictionary'
target_word_to_senses_domain_path = '../resources/target_word_to_senses_domain.dictionary'
target_word_to_senses_lex_path = '../resources/target_word_to_senses_lex.dictionary'

import time, random


class MemNNConfig(object):
    """MemNN+ parameters"""

    def __init__(self):
        # parameters for mask
        self.maximun_sense = 5
        self.vocab_size = 37176
        self.sense_num = 0
        self.pos_num = 0
        self.lex_num = 0
        
        # parameters for pre-trained word embedding
        self.embedding_size = 300
        self.use_pre_trained_embedding = True #True

        # parameters for input data
        self.n_step_f = 30  # forward context length
        self.n_step_b = 30  # backward context length

        # parameters for knowledge(glosses)
        self.gloss_expand_type = 2  # 0=None 1=hyper，2=hypo 3=hyper+hypo 4：hierarchical
        self.max_gloss_words = 100
        self.max_n_sense = 10
        self.min_sense_freq = 1
        self.maximum_setence_length = 55

        # parameters for model
        self.n_lstm_units = 1024 #1024
        self.forget_bias = 0.0
        self.memory_update_type = 'concat'  # 'concat' or 'linear'
        self.memory_hop = 1
        self.concat_target_gloss = False
        self.keep_prob = 0.5  # dropout keep_prob

        # parameters for train
        self.n_epochs = 50
        self.batch_size = 10
        self.lr_start = 0.9 # 0.1
        self.minimum_lr = 0.001
        self.lambda_l2_reg = 0.01  # L2 regularization
        self.momentum = 0.8
        self.max_grad_norm = 10  # clip gradient

        # Validation info
        self.evaluate_gap = 1000
        self.store_log_gap = 100
        self.validate = True
        self.min_no_improvement = 10
        self.print_batch = True

        self.batch_norm = False
    def set_vocab_size(self, size):
        self.vocab_size = size
        
    def set_maximum_setence_length(self, size):
        self.maximum_setence_length = size
    
    def set_pos_size(self, size):
        self.pos_num = size
    
    def set_lex_size(self, size):
        self.lex_num = size
        
    def set_sense_num(self, size):
        self.sense_num = size
    def random_config(self):
        random.seed(time.time())
        # self.memory_update_type = random.choice(['concat', 'linear'])
        # self.memory_hop = random.choice([1, 2, 3])
        self.n_lstm_units = random.choice([128, 256, 512])
        self.max_n_sense = random.choice([10, 20, 40])
        self.min_sense_freq = random.choice([1, 3, 5, 10])
        # self.gloss_emb_size = random.choice([64, 128, 256])
        # self.lr_start = random.choice([0.01, 0.001])
