import nltk
nltk.download('wordnet')
from data import *
from model_basic_elmo import Model
from glove import *
import path
import tensorflow as tf
import numpy as np
from nltk.corpus import wordnet
import os
import tensorflow_hub



def feed_dict(batch_data,model, is_training=True):
    context,sense_mask ,label,origin_data, total_target_number, maximum_potential_sense_number, maximum_target_number, maximum_setence_length = batch_data
    '''
    self.is_training = tf.placeholder(tf.bool, name='is_training')

    self.inputs = tf.placeholder(tf.int32, shape=[batch_size, n_step_f + n_step_b + 1], name='inputs')
    self.target_ids = tf.placeholder(tf.int32, shape=[batch_size, None], name='target_ids')
    self.sense_ids = tf.placeholder(tf.int32, shape=[batch_size, None], name='sense_ids')
    self.tot_n_senses = tf.placeholder(tf.int32, shape=[], name='tot_n_senses')
    '''
    feeds = {
        model.is_training: is_training,  # control keep_prob for dropout
        model.inputs: context,
        model.sense_mask: sense_mask,
        model.sense_ids: label,
#        model.tot_n_senses:total_target_number
    }
    return feeds

def evaluate_target_words(batch_data, predictions, mfs, id_to_sense, wn_to_babelnet = None, babelnet_to_domain = None):
    context,sense_mask ,label,origin_data, total_target_number, maximum_potential_sense_number, maximum_target_number, maximum_setence_length = batch_data
    
    target_id_list = []
    result_index = []
    target_wordss = []
    contenxt_lists = ['<pad>'] * maximum_setence_length * len(batch_data)
    for i in range(len(origin_data)):
        start_pos = i * maximum_setence_length
        target_word_pos_id_list = origin_data[i]['target_word_pos_id_list']
        target_word = origin_data[i]['target_word']
        target_id = origin_data[i]['target_id']
        
        contenxt = origin_data[i]['context']
        target_wordss += target_word
        result_index += [val + start_pos for val in target_word_pos_id_list]
        target_id_list += target_id
        contenxt_lists[start_pos:len(contenxt)] = contenxt
    predictions = np.array(predictions).reshape(-1,1)
    predicts = predictions[result_index,:].reshape(-1)
    result = []
    #print('finish extracting the information')
    num1 = 0
    num2 = 0
    num3 = 0
    for index,val in enumerate(predicts):
        i = result_index[index]
        if contenxt_lists[i] == '<unk>' or val not in id_to_sense or id_to_sense[val] == '<unk>':
            if target_wordss[index] not in mfs:
                my_word = target_wordss[index].split('#')[0]
                tmp = target_id_list[index]  + ' '
                sensekey = wordnet.synsets(my_word)[0].lemmas()[0].key()
                if wn_to_babelnet and babelnet_to_domain:
                    synset = wordnet.lemma_from_key(sensekey).synset()
                    synset_id = "wn:" + str(synset.offset()).zfill(8) + synset.pos()
                    if synset_id in wn_to_babelnet:
                            babelnet_id = wn_to_babelnet[synset_id]
                            if babelnet_id in babelnet_to_domain:
                                domain = babelnet_to_domain[babelnet_id]
                            else:
                                domain = '<unk>'
                    else:
                        domain = '<unk>'
                    tmp += domain
                else:
                    tmp += sensekey
                num1 += 1
            else:
                tmp = target_id_list[index] + ' ' + mfs[target_wordss[index]][0]
                num2 += 1
        else:
            tmp = target_id_list[index] + ' ' + id_to_sense[val]
            num3 += 1
        result.append(tmp)
    #print(num1, num2, num3)
    
    return result

def predict_babelnet(input_path : str, output_path : str, resources_path : str) -> None:
    """
    DO NOT MODIFY THE SIGNATURE!
    This is the skeleton of the prediction function.
    The predict function will build your model, load the weights from the checkpoint and write a new file (output_path)
    with your predictions in the "<id> <BABELSynset>" format (e.g. "d000.s000.t000 bn:01234567n").
    
    The resources folder should contain everything you need to make the predictions. It is the "resources" folder in your submission.
    
    N.B. DO NOT HARD CODE PATHS IN HERE. Use resource_path instead, otherwise we will not be able to run the code.
    If you don't know what HARD CODING means see: https://en.wikipedia.org/wiki/Hard_coding

    :param input_path: the path of the input file to predict in the same format as Raganato's framework (XML files you downloaded).
    :param output_path: the path of the output file (where you save your predictions)
    :param resources_path: the path of the resources folder containing your model and stuff you might need.
    :return: None
    """
    print('loading the model')
    mfs = load_obj(mfs_path)
    target_word_to_senses = load_obj(target_word_to_senses_path)
    word_to_id = load_obj(word_to_id_path)
    sense_to_id = load_obj(sense_to_id_path)
    
    
    #config = MemNNConfig()
    val_data = load_all_words_coarse_data(Dev_data_path, None, babelnet2wordnet, babelnet2wndomains,word_to_id=word_to_id)
    
    
    id_to_sense = {j:i for i,j in sense_to_id.items()}
    config.set_vocab_size(len(word_to_id))
    config.set_sense_num(len(sense_to_id))
    # === Initial model
    tf.reset_default_graph()
    model = Model(config, None, None)
    print('loading the model finish')
    # === Create session
    tf_config = tf.ConfigProto()
    tf_config.gpu_options.allow_growth = True  # allocate dynamically
    tf_config.gpu_options.per_process_gpu_memory_fraction = 1  # maximun alloc gpu50% of MEM
    session = tf.Session(config=tf_config)
    #new_saver = tf.train.import_meta_graph('../resources/model/Basic_model_elmo_0.9_1024_0.5_0.9_5_0/-79887.meta')
    new_saver = tf.train.Saver(max_to_keep=5)
    new_saver.restore(session, tf.train.latest_checkpoint(resources_path))
    
    pos_num = 0
    target_num = 0
    
    
    val_data_generate = batch_generator(False, config.batch_size, val_data, 1, target_word_to_senses, word_to_id,sense_to_id, pad_last_batch=True, elmo=True)
    
    total_result = []
    for batch_id, batch_data in enumerate(val_data_generate):
        if batch_id % 50 == 0:
            print('I am processing ', batch_id)
        feeds = feed_dict(batch_data,model, is_training=False)
        predictions = \
            session.run([model.predicted_sense], feeds)
        result = evaluate_target_words(batch_data, predictions, mfs, id_to_sense)
        total_result += result            
#    output_path = '../resources/prediction.txt'
    file = open(output_path,'w')
    for i in total_result:
        file.write(i+'\n')
    file.close()
    
def predict_wordnet_domains(input_path : str, output_path : str, resources_path : str) -> None:
    """
    DO NOT MODIFY THE SIGNATURE!
    This is the skeleton of the prediction function.
    The predict function will build your model, load the weights from the checkpoint and write a new file (output_path)
    with your predictions in the "<id> <wordnetDomain>" format (e.g. "d000.s000.t000 sport").

    The resources folder should contain everything you need to make the predictions. It is the "resources" folder in your submission.

    N.B. DO NOT HARD CODE PATHS IN HERE. Use resource_path instead, otherwise we will not be able to run the code.
    If you don't know what HARD CODING means see: https://en.wikipedia.org/wiki/Hard_coding

    :param input_path: the path of the input file to predict in the same format as Raganato's framework (XML files you downloaded).
    :param output_path: the path of the output file (where you save your predictions)
    :param resources_path: the path of the resources folder containing your model and stuff you might need.
    :return: None
    """
    wn_to_babelnet = {}
    if babelnet2wordnet:
        babelnet_to_wn = load_mapping(babelnet2wordnet)
        for key, val in babelnet_to_wn.items():
            wn_to_babelnet[val] = key
    
    babelnet_to_domain = {}
    if babelnet2wndomains:
        babelnet_to_domain = load_mapping(babelnet2wndomains)
        
    mfs = load_obj(mfs_domain_path)
    config = MemNNConfig()
    val_data = load_all_words_coarse_data(input_path, None, babelnet2wordnet, babelnet2wndomains, coarse=True)
    target_word_to_senses = load_obj(target_word_to_senses_domain_path)
    word_to_id = load_obj(word_to_id_path)
    sense_to_id = load_obj(sense_to_id_domain_path)
    
    id_to_sense = {j:i for i,j in sense_to_id.items()}
    config.set_vocab_size(len(word_to_id))
    config.set_sense_num(len(sense_to_id))
    # === Initial model
    tf.reset_default_graph()
    model = Model(config, None, None)
    
    # === Create session
    tf_config = tf.ConfigProto()
    tf_config.gpu_options.allow_growth = True  # allocate dynamically
    tf_config.gpu_options.per_process_gpu_memory_fraction = 1  # maximun alloc gpu50% of MEM
    session = tf.Session(config=tf_config)
    #new_saver = tf.train.import_meta_graph('../resources/model/Basic_model_elmo_0.9_1024_0.5_0.9_5_0/-79887.meta')
    new_saver = tf.train.Saver(max_to_keep=5)
    new_saver.restore(session, tf.train.latest_checkpoint(resources_path))
    
    pos_num = 0
    target_num = 0
    
    
    val_data_generate = batch_generator(False, config.batch_size, val_data, 1, target_word_to_senses, word_to_id,sense_to_id, pad_last_batch=True, elmo=True)
    
    total_result = []
    for batch_id, batch_data in enumerate(val_data_generate):
        if batch_id % 50 == 0:
            print('I am processing ', batch_id)
        feeds = feed_dict(batch_data,model, is_training=False)
        predictions = \
            session.run([model.predicted_sense], feeds)
        result = evaluate_target_words(batch_data, predictions, mfs, id_to_sense, wn_to_babelnet = wn_to_babelnet, babelnet_to_domain = babelnet_to_domain)
        total_result += result            
#    output_path = '../resources/prediction.txt'
    file = open(output_path,'w')
    for i in total_result:
        file.write(i+'\n')
    file.close()


def predict_lexicographer(input_path : str, output_path : str, resources_path : str) -> None:
    """
    DO NOT MODIFY THE SIGNATURE!
    This is the skeleton of the prediction function.
    The predict function will build your model, load the weights from the checkpoint and write a new file (output_path)
    with your predictions in the "<id> <lexicographerId>" format (e.g. "d000.s000.t000 noun.animal").

    The resources folder should contain everything you need to make the predictions. It is the "resources" folder in your submission.

    N.B. DO NOT HARD CODE PATHS IN HERE. Use resource_path instead, otherwise we will not be able to run the code.
    If you don't know what HARD CODING means see: https://en.wikipedia.org/wiki/Hard_coding

    :param input_path: the path of the input file to predict in the same format as Raganato's framework (XML files you downloaded).
    :param output_path: the path of the output file (where you save your predictions)
    :param resources_path: the path of the resources folder containing your model and stuff you might need.
    :return: None
    """
    print('loading the model')
    wn_to_babelnet = {}
    if babelnet2wordnet:
        babelnet_to_wn = load_mapping(babelnet2wordnet)
        for key, val in babelnet_to_wn.items():
            wn_to_babelnet[val] = key
    
    babelnet_to_domain = {}
    if babelnet2lexnames:
        babelnet_to_domain = load_mapping(babelnet2lexnames)

    mfs = load_obj(mfs_lex_path)
    config = MemNNConfig()
    val_data = load_all_words_coarse_data(input_path, None, babelnet2wordnet, babelnet2lexnames, coarse=True)
    target_word_to_senses = load_obj(target_word_to_senses_lex_path)
    word_to_id = load_obj(word_to_id_path)
    sense_to_id = load_obj(sense_to_id_lex_path)
    
    id_to_sense = {j:i for i,j in sense_to_id.items()}
    config.set_vocab_size(len(word_to_id))
    config.set_sense_num(len(sense_to_id))
    # === Initial model
    tf.reset_default_graph()
    model = Model(config, None, None)
    
    # === Create session
    tf_config = tf.ConfigProto()
    tf_config.gpu_options.allow_growth = True  # allocate dynamically
    tf_config.gpu_options.per_process_gpu_memory_fraction = 1  # maximun alloc gpu50% of MEM
    session = tf.Session(config=tf_config)
    #new_saver = tf.train.import_meta_graph('../resources/model/Basic_model_elmo_0.9_1024_0.5_0.9_5_0/-79887.meta')
    new_saver = tf.train.Saver(max_to_keep=5)
    new_saver.restore(session, tf.train.latest_checkpoint(resources_path))
    
    print('finish loading')
    pos_num = 0
    target_num = 0
    
    
    val_data_generate = batch_generator(False, config.batch_size, val_data, 1, target_word_to_senses, word_to_id,sense_to_id, pad_last_batch=True, elmo=True)
    
    print('start the prediction procedure')
    total_result = []
    for batch_id, batch_data in enumerate(val_data_generate):
        feeds = feed_dict(batch_data,model, is_training=False)
        predictions = \
            session.run([model.predicted_sense], feeds)
        result = evaluate_target_words(batch_data, predictions, mfs, id_to_sense, wn_to_babelnet = wn_to_babelnet, babelnet_to_domain = babelnet_to_domain)
        total_result += result            
    print('finish the predict part')
#    output_path = '../resources/prediction.txt'
    file = open(output_path,'w')
    for i in total_result:
        file.write(i+'\n')
    file.close()
predict_babelnet(Dev_data_path, '../predict-1_test.txt', '../resources/Basic_model_elmo_0.9_1024_0.5_0.8_5_3_fix_bug_in_preprocessing-1')
predict_wordnet_domains(Dev_data_path, '../predict_domain_test.txt', '../resources/Basic_model_elmo_0.9_1024_0.5_0.8_5_3_fix_bug_in_preprocessing_coarse')
predict_lexicographer(Dev_data_path, '../predict_lex_test.txt', '../resources/Basic_model_elmo_0.9_1024_0.5_0.8_5_3_fix_bug_in_preprocessing_coarse_task3')